#!/bin/sh
# wait-for-postgres.sh

set -e

host="$1"
MigrationDB="$2"
shift 2
cmd="$@"
export POSTGRES_PASSWORD=$(cat /run/secrets/dbpass)
export POSTGRES_USER=$(cat /run/secrets/dbuser)
export POSTGRES_DB=$(cat /run/secrets/dbname)
export POSTGRESQL_URL=postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@db/$POSTGRES_DB
until PGPASSWORD=$POSTGRES_PASSWORD PGUSER=$POSTGRES_USER PGDATABASE=$POSTGRES_DB psql -h "$host" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - executing command"
if [[ $MigrationDB = "migrationdb-on" ]]; then 

flask $host upgrade
echo "Migration DB ON"
     else
          echo "Migration DB OFF"
     fi
exec $cmd