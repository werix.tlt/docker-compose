# аргументы
ARG APP_IMAGE=python:3.6-alpine
ARG USER=app
ARG GROUP=app
# базовый образ
FROM $APP_IMAGE AS base
FROM base as builder

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /usr/src/app
COPY ./requirements.txt .

# установка зависимостей
RUN apk update \
	&& apk add postgresql-dev gcc python3-dev musl-dev \
	&& pip install --upgrade pip \
	&& pip wheel --no-cache-dir --no-deps --wheel-dir /usr/src/app/wheels -r requirements.txt --pre

# готовый образ
FROM base
ARG USER
ARG GROUP
ENV APP_DIR=/home/$USER/app
ENV FLASK_APP app.py
WORKDIR $APP_DIR

# установка зависимостей и копирование из базового образа / создание пользователя
COPY --from=builder /usr/src/app/wheels /wheels

# создание пользователя + установка зависимостей и копирование из базового образа
RUN addgroup -S $GROUP && adduser -S $USER -G $GROUP \
	&&apk update && apk add postgresql-client && pip install --no-cache /wheels/*

# копирование приложения
COPY . $APP_DIR

# изменение прав для пользователя + права на выполнение скрипта
RUN chown -R $USER:$GROUP $APP_DIR && chmod +x wait-for-postgres.sh

# изменение пользователя
USER $USER

EXPOSE 5000

#запуск приложения
ENTRYPOINT ["./wait-for-postgres.sh"]